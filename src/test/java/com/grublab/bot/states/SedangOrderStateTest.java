package com.grublab.bot.states;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.services.MenuService;
import com.grublab.bot.services.MessageService;
import com.grublab.bot.services.MessageServiceImpl;
import com.grublab.bot.services.StateService;
import com.linecorp.bot.model.message.Message;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SedangOrderStateTest {

    @Mock
    private StateService stateService;

    @Mock
    private MenuService menuService;

    @Mock
    private MessageService messageService;

    @InjectMocks
    SedangOrderState sedangOrderState;

    private Pesanan pesanan;
    private String idLine;
    private Menu menu;

    @BeforeEach
    void setUp() {
        menu = new Menu("Ayam Goreng", "Ayam Digoreng", 10000);

        idLine = "userX";
        pesanan = new Pesanan(idLine);
        pesanan.addMenu(menu);

        stateService = mock(StateService.class);
        messageService = new MessageServiceImpl();
        sedangOrderState = new SedangOrderState(pesanan);

        sedangOrderState.setStateService(stateService);
        sedangOrderState.setMessageService(messageService);
        sedangOrderState.setMenuService(menuService);
    }

    @Test
    void testHelpMengembalikanListMenu() {
        when(menuService.getMenus()).thenReturn(Arrays.asList(menu));
        List <Message> response = sedangOrderState.help(idLine);
        assertNotNull(response);
        assertEquals(2, response.size());
    }

    @Test
    void testPesanMengembalikanListMessage() {
        List<Message> response = sedangOrderState.pesan(idLine);
        assertNotNull(response);
        assertEquals(1, response.size());
    }

    @Test
    void testKembaliMengembalikanListMessage() {
        List<Message> response = sedangOrderState.kembali(idLine);
        assertNotNull(response);
        assertEquals(2, response.size());
    }

    @Test
    void testBatalMengembalikanResponse() {
        List<Message> response = sedangOrderState.batal(idLine);
        assertNotNull(response);
        assertEquals(2, response.size());
    }

    @Test
    void testCekPromoMengembalikanListMessage() {
        List<Message> response = sedangOrderState.cekPromo(idLine);
        assertNotNull(response);
        assertEquals(1, response.size());
    }


    @Test
    void testTambahPesananTersimpanSesuaiDanMengembalikanResponse() {
        when(menuService.getMenuByName("Ayam Goreng")).thenReturn(menu);
        List<Message> response = sedangOrderState.tambahPesanan(idLine, "Ayam Goreng");
        assertNotNull(response);
        assertEquals(1, response.size());

        List<Menu> orderlist = sedangOrderState.pesanan.getOrderList();
        assertNotNull(orderlist);
        assertEquals(2, orderlist.size());
    }

    @Test
    void testHapusBerhasilMenghapusItemMenuDariPesananDanMengembalikanResponse() {
        String menu = "Ayam Goreng";

        List<Menu> orderlist = pesanan.getOrderList();
        assertEquals(1, orderlist.size());

        List<Message> response = sedangOrderState.hapusPesanan(idLine, menu);
        assertNotNull(response);
        assertEquals(1, response.size());

        assertEquals(0, orderlist.size());
    }

    @Test
    void testGunakanPromoMengembalikanListMessage() {
        List<Message> response = sedangOrderState.gunakanPromo(idLine, "promoX");
        assertNotNull(response);
        assertEquals(1, response.size());
    }

    @Test
    void testHapusPromoMengembalikanListMessage() {
        List<Message> response = sedangOrderState.hapusPromo(idLine);
        assertNotNull(response);
        assertEquals(1, response.size());
    }

    @Test
    void testSendKonfirmasiMengembalikanResponse() {
        List <Message> response = sedangOrderState.sendKonfirmasi(idLine);
        assertNotNull(response);
        assertEquals(2, response.size());
    }

    @Test
    void testKonfirmasiMengembalikanResponse(){
        List <Message> response = sedangOrderState.konfirmasi(idLine);
        assertNotNull(response);
        assertEquals(3, response.size());
    }

    @Test
    void testKonfirmasiBatalMengembalikannRespone() {
        List <Message> responses = sedangOrderState.batalConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testKonfirmasiKembaliMengembalikanResponse() {
        List<Message> responses = sedangOrderState.kembaliConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void  testCekKeranjangMengembalikanResponse() {
        List <Message> response = sedangOrderState.cekKeranjang(idLine);
        assertNotNull(response);
        assertEquals(1, response.size());
    }

}
