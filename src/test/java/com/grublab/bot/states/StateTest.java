package com.grublab.bot.states;

import com.grublab.bot.models.Customer;
import com.grublab.bot.models.Menu;
import com.grublab.bot.models.PesananHistory;
import com.grublab.bot.services.PesananService;
import com.grublab.bot.services.UserService;
import com.linecorp.bot.model.message.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StateTest {

    private  Class<?> stateClass;

    private State state;

    private String idLine;

    @Mock
    private UserService userService;

    @Mock
    private PesananService pesananService;

    private Customer customer;

    private List<PesananHistory> pesananHistories;

    private PesananHistory pesananHistory;

    @BeforeEach
    void setUp() throws Exception {
        stateClass = Class.forName("com.grublab.bot.states.State");
        state = new BelumOrderState();
        idLine = "aiueie123913";

        customer = new Customer();
        customer.setIdLine("aiueie123913");
        customer.setDisplayName("Alif Saddid");
        customer.setPoint(0);

        state.setUserService(userService);
        state.setPesananService(pesananService);

        pesananHistory = new PesananHistory();
        pesananHistory.setMenus(new ArrayList<>());
        pesananHistory.getMenus().add(new Menu("Ayam Goreng", "Ayam Digoreng", 10000));

        pesananHistories = new ArrayList<>();
        pesananHistories.add(pesananHistory);
    }

    @Test
    void testCekHistoryShouldReturnHistory() {
        when(pesananService.getCustomerOrderHistory(idLine)).thenReturn(pesananHistories);
        List<Message> responses = state.cekHistory(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testCekProfilShouldReturnProfile() {
        when(userService.getOrCreateCustomerByIdLine(idLine)).thenReturn(customer);
        List<Message> responses = state.cekProfil(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testStateIsAPublicAbstractClass() {
        int classModifiers = stateClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

}
