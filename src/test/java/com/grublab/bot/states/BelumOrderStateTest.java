package com.grublab.bot.states;

import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.services.*;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BelumOrderStateTest {
    private Pesanan pesanan;
    private State belumOrderState;
    private String idLine;

    @Mock
    private MenuService menuService;

    @Mock
    private StateService stateService;

    @Mock
    private MessageService messageService;

    private Menu menu;

    @BeforeEach
    void setUp(){
        menu = new Menu("Ayam Goreng", "Ayam Digoreng", 10000);

        idLine = "wkwkwk";
        pesanan = new Pesanan(idLine);
        pesanan.addMenu(new Menu("Ayam Goreng", "Ayam Digoreng", 10000));

        stateService = mock(StateService.class);
        messageService = new MessageServiceImpl();
        belumOrderState = new BelumOrderState();

        belumOrderState.setStateService(stateService);
        belumOrderState.setMessageService(messageService);
        belumOrderState.setMenuService(menuService);
    }

    @Test
    void testHelpMethodReturnResponses(){
        List <Message> responses = belumOrderState.help(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testPesanMethodResponsesAndChangeUserState(){
        when(menuService.getMenus()).thenReturn(Arrays.asList(menu));
        List <Message> responses = belumOrderState.pesan(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());

        verify(stateService, times(1)).setUserState(any(), any());
    }

    @Test
    void testTambahPesananReturnResponses(){
        List <Message> responses = belumOrderState.tambahPesanan(idLine, "AyamGoreng");
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testHapusPesananReturnResponses(){
        List <Message> responses = belumOrderState.hapusPesanan(idLine, "Ayam");
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testSendKonfirmasiReturnResponses(){
        List <Message> responses = belumOrderState.sendKonfirmasi(idLine);
        assertNotNull(responses);
        assertEquals(3, responses.size());
    }

    @Test
    void testKonfirmasiReturnResponses(){
        List <Message> responses = belumOrderState.konfirmasi(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testBatalReturnResponses(){
        List <Message> responses = belumOrderState.batal(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testKembaliReturnResponses(){
        List <Message> responses = belumOrderState.kembali(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testCekPromoReturnResponses(){
        List <Message> responses = belumOrderState.cekPromo(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testGunakanPromoReturnResponses(){
        List <Message> responses = belumOrderState.gunakanPromo(idLine, "Promo Lebaran");
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testHapusPromoReturnResponses(){
        List <Message> responses = belumOrderState.hapusPromo(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testBatalConfirmationShouldReturnMessage() {
        List <Message> responses = belumOrderState.batalConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testKembaliConfirmationShouldReturnMessage() {
        List <Message> responses = belumOrderState.kembaliConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testCekKeranjangReturnResponses(){
        List <Message> responses = belumOrderState.cekKeranjang(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }
}