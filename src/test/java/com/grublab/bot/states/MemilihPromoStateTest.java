package com.grublab.bot.states;

import com.grublab.bot.models.Customer;
import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.promo.NoPromo;
import com.grublab.bot.promo.PromoLebaran;
import com.grublab.bot.services.MenuService;
import com.grublab.bot.services.MessageService;
import com.grublab.bot.services.StateService;
import com.grublab.bot.services.UserService;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MemilihPromoStateTest {

    private Pesanan pesanan;
    private State memilihPromoState;
    private String idLine;
    private String promo;
    private Customer customer;

    @Mock
    private StateService stateService;

    @Mock
    private MessageService messageService;

    @Mock
    private UserService userService;

    @Mock
    private MenuService menuService;

    @BeforeEach
    void setUp() {
        promo = "Promo Di Rumah Aja";
        idLine = "eiui12u318321u";

        pesanan = new Pesanan(idLine);
        pesanan.addMenu(new Menu("Ayam Goreng", "Ayam Digoreng", 10000));

        stateService = mock(StateService.class);
        messageService = mock(MessageService.class);

        memilihPromoState = new MemilihPromoState(pesanan);
        memilihPromoState.setMessageService(messageService);
        memilihPromoState.setStateService(stateService);
        memilihPromoState.setUserService(userService);
        memilihPromoState.setMenuService(menuService);

        customer = new Customer();
        customer.setIdLine(idLine);
        customer.setPoint(1000);
    }

    @Test
    void testHelpShouldReturnMenuAndKonfirmasiButton() {
        List<Message> responses = memilihPromoState.help(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testPesanShouldReturnATextMessage() {
        List<Message> responses = memilihPromoState.pesan(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
        assertTrue(responses.get(0) instanceof TextMessage);
    }

    @Test
    void testTambahPesananShouldReturnATextMessage() {
        List<Message> responses = memilihPromoState.tambahPesanan(idLine, "Ayam Goreng");
        assertNotNull(responses);
        assertEquals(1, responses.size());
        assertTrue(responses.get(0) instanceof TextMessage);
    }

    @Test
    void testHapusPesananShouldReturnATextMessage() {
        List<Message> responses = memilihPromoState.hapusPesanan(idLine, "Ayam Goreng");
        assertNotNull(responses);
        assertEquals(1, responses.size());
        assertTrue(responses.get(0) instanceof TextMessage);
    }

    @Test
    void testSendKonfirmasiShouldReturnKeranjangAndKonfirmasiMessage() {
        List<Message> responses = memilihPromoState.sendKonfirmasi(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testKonfirmasiShouldChangeUserState() {
        HashMap<String, State> stateRepository = new HashMap<String, State>();

        doAnswer(new Answer() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                String idLine = (String)args[0];
                State state = (OrderSedangDibuatState)args[1];
                stateRepository.put(idLine, state);
                return null;
            }
        }).when(stateService).setUserState(any(), any());
        when(userService.getOrCreateCustomerByIdLine(idLine)).thenReturn(customer);
        List<Message> responses = memilihPromoState.konfirmasi(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
        assertTrue(responses.get(0) instanceof TextMessage);
        assertTrue(stateRepository.get(idLine) instanceof OrderSedangDibuatState);

        verify(stateService, times(1)).setUserState(any(), any());
        verify(messageService, times(1)).sendMessageToSeller(any());
    }

    @Test
    void testCekPromoShouldReturnPromoList() {
        List<Message> responses = memilihPromoState.cekPromo(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testGunakanPromoShouldSetPromo() {
        when(userService.getOrCreateCustomerByIdLine(idLine)).thenReturn(customer);
        List<Message> responses = memilihPromoState.gunakanPromo(idLine, "Promo Lebaran");

        Pesanan pesanan = memilihPromoState.pesanan;
        assertTrue(pesanan.getPromo() instanceof PromoLebaran);
        assertNotNull(responses);
        assertEquals(1, responses.size());
        assertTrue(responses.get(0) instanceof TextMessage);
    }

    @Test
    void testHapusPromoShouldDeletePromo() {
        List<Message> responses = memilihPromoState.hapusPromo(idLine);

        Pesanan pesanan = memilihPromoState.pesanan;
        assertTrue(pesanan.getPromo() instanceof NoPromo);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testGunakanPromoPointKurangShouldNotSetPromo() {
        customer.setPoint(0);
        when(userService.getOrCreateCustomerByIdLine(idLine)).thenReturn(customer);
        List<Message> responses = memilihPromoState.gunakanPromo(idLine, "Promo Lebaran");

        Pesanan pesanan = memilihPromoState.pesanan;
        assertTrue(pesanan.getPromo() instanceof NoPromo);
        assertNotNull(responses);
        assertEquals(1, responses.size());
        assertTrue(responses.get(0) instanceof TextMessage);
    }

    @Test
    void testBatalShouldCancelOrder() {
        doNothing().when(stateService).setUserState(any(), any());
        List<Message> responses = memilihPromoState.batal(idLine);

        assertNotNull(responses);
        assertEquals(2, responses.size());
    }

    @Test
    void testBatalConfirmationShouldReturnConfirmationBox() {
        List <Message> responses = memilihPromoState.batalConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testKembaliConfirmationShouldReturnConfirmationBox() {
        List <Message> responses = memilihPromoState.kembaliConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testCekKeranjangShouldReturnKeranjang() {
        List<Message> responses = memilihPromoState.cekKeranjang(idLine);

        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testCekKembaliShouldBackToSedangOrderState() {
        doNothing().when(stateService).setUserState(any(),any());
        when(menuService.getMenus()).thenReturn(Arrays.asList(pesanan.getOrderList().get(0)));
        List<Message> responses = memilihPromoState.kembali(idLine);

        assertNotNull(responses);
        assertEquals(3, responses.size());
        assertTrue(responses.get(0) instanceof TextMessage);
    }
}
