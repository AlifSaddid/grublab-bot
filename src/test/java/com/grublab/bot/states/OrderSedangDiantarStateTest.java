package com.grublab.bot.states;

import com.grublab.bot.models.Customer;
import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.promo.NoPromo;
import com.grublab.bot.services.MessageService;
import com.grublab.bot.services.PesananService;
import com.grublab.bot.services.StateService;
import com.grublab.bot.services.UserService;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderSedangDiantarStateTest {
    private Pesanan pesanan;
    private State orderSedangDiantar;
    private String idLine;
    private String promo;

    @Mock
    private StateService stateService;

    @Mock
    private MessageService messageService;

    @Mock
    private UserService userService;

    @Mock
    private PesananService pesananService;

    private Customer customer;

    @BeforeEach
    void setUp() {
        promo = "Promo Lebaran";
        idLine = "asikuhuy12";

        pesanan = new Pesanan(idLine);
        pesanan.addMenu(new Menu("Ayam Bakar", "Ayam Dibakar", 10000));
        pesanan.setPromo(new NoPromo());

        userService = mock(UserService.class);

        orderSedangDiantar = new OrderSedangDiantarState(pesanan);
        orderSedangDiantar.setMessageService(messageService);
        orderSedangDiantar.setStateService(stateService);
        orderSedangDiantar.setUserService(userService);
        orderSedangDiantar.setPesananService(pesananService);

        orderSedangDiantar.setPesanan(pesanan);

        customer = new Customer();
        customer.setIdLine(idLine);
        customer.setPoint(100);
    }

    @Test
    void testHelpShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.help(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testPesanShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.pesan(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testtambahPesananShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.tambahPesanan(idLine, "Ayam Bakar");
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testhapusPesananShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.hapusPesanan(idLine, "Ayam Bakar");
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testcekHistoryShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.cekHistory(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testcekProfilShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.cekProfil(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testbatalShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.batal(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testkembaliShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.kembali(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testcekPromoShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.cekPromo(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testgunakanPromoShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.gunakanPromo(idLine, "Promo Lebaran");
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testhapusPromoShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.hapusPromo(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testcekKeranjangShouldReturnResponse() {
        List<Message> responses = orderSedangDiantar.cekKeranjang(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testCekKonfirmasiShouldBackToBelumOrderState(){
        when(userService.getOrCreateCustomerByIdLine(idLine)).thenReturn(customer);
        doNothing().when(userService).updateCustomerPoint(idLine, customer.getPoint() + pesanan.getPriceWithPromo() / 100 - pesanan.getPromo().getCost());
        doNothing().when(pesananService).savePesanan(pesanan, idLine);

        List <Message> responses = orderSedangDiantar.konfirmasi(idLine);

        assertNotNull(responses);
        assertEquals(3, responses.size());

    }

    @Test
    void testBatalConfirmationShouldReturnMessage() {
        List <Message> responses = orderSedangDiantar.batalConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testKembaliConfirmationShouldReturnMessage() {
        List <Message> responses = orderSedangDiantar.kembaliConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testsendKonfirmasiShouldReturnTestAndKonfirmasiButton() {
        List<Message> responses = orderSedangDiantar.sendKonfirmasi(idLine);
        assertNotNull(responses);
        assertEquals(2, responses.size());
    }


}