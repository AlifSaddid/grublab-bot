package com.grublab.bot.states;

import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.services.MessageService;
import com.grublab.bot.services.StateService;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderSedangDibuatStateTest {
    private Pesanan pesanan;
    private State orderSedangDibuat;
    private String idLine;
    private String promo;

    @Mock
    private StateService stateService;

    @Mock
    private MessageService messageService;

    @BeforeEach
    void setUp() {
        promo = "Promo Ramadhan";
        idLine = "limahuruf";

        pesanan = new Pesanan(idLine);
        pesanan.addMenu(new Menu("Ayam Bakar", "Ayam Dibakar", 25000));

        stateService = mock(StateService.class);
        messageService = mock(MessageService.class);

        orderSedangDibuat = new OrderSedangDibuatState(pesanan);
        orderSedangDibuat.setMessageService(messageService);
        orderSedangDibuat.setStateService(stateService);
    }

    @Test
    void testHelpShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.help(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testPesanShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.pesan(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testtambahPesananShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.tambahPesanan(idLine, "Ayam Bakar");
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testhapusPesananShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.hapusPesanan(idLine, "Ayam Bakar");
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testSendKonfirmasiReturnResponses(){
        List <Message> responses = orderSedangDibuat.sendKonfirmasi(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testKonfirmasiMengembalikanResponse(){
        List <Message> response = orderSedangDibuat.konfirmasi(idLine);
        assertNotNull(response);
        assertEquals(1, response.size());
    }

    @Test
    void testbatalShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.batal(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testkembaliShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.kembali(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testcekPromoShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.cekPromo(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testgunakanPromoShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.gunakanPromo(idLine, "Promo Ramadhan");
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testhapusPromoShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.hapusPromo(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testBatalConfirmationShouldReturnMessage() {
        List <Message> responses = orderSedangDibuat.batalConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testKembaliConfirmationShouldReturnMessage() {
        List <Message> responses = orderSedangDibuat.kembaliConfirmation(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

    @Test
    void testcekKeranjangShouldReturnResponse() {
        List<Message> responses = orderSedangDibuat.cekKeranjang(idLine);
        assertNotNull(responses);
        assertEquals(1, responses.size());
    }

}
