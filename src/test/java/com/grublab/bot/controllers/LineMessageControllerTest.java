package com.grublab.bot.controllers;

import com.grublab.bot.services.MessageService;
import com.grublab.bot.services.MessageServiceImpl;
import com.grublab.bot.services.StateService;
import com.grublab.bot.services.StateServiceImpl;
import com.grublab.bot.states.BelumOrderState;
import com.grublab.bot.states.State;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.response.BotApiResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LineMessageControllerTest {

    @Mock
    LineMessagingClient lineMessagingClient;

    @Mock
    StateService stateService;

    @Mock
    MessageService messageService;

    @Mock
    CompletableFuture<BotApiResponse> botApiCF;

    @Mock
    State stateMock;

    private LineMessageController lineMessageController;

    private String idLine;

    private ArrayList<String> details;

    private State state;

    @BeforeEach
    void setUp() {
        idLine = "eieyoadsh91293";
        lineMessagingClient = mock(LineMessagingClient.class);
        stateService = mock(StateServiceImpl.class);
        messageService = mock(MessageServiceImpl.class);
        stateMock = mock(BelumOrderState.class);

        lineMessageController = new LineMessageController(lineMessagingClient,
                stateService,
                messageService);

        state = new BelumOrderState();
    }

    private MessageEvent<TextMessageContent> createDummyMessageEvent(String text, String userId) {
        return MessageEvent.<TextMessageContent>builder()
                .replyToken("replyToken")
                .timestamp(Instant.now())
                .source(
                        UserSource.builder()
                                .userId(userId)
                                .build()
                )
                .message(
                        TextMessageContent.builder()
                                .id("id")
                                .text(text)
                                .build()
                )
                .build();
    }

    private PostbackEvent createDummyPostbackEvent(String data, String userId) {
        return PostbackEvent.builder()
                .timestamp(Instant.now())
                .source(
                        UserSource.builder()
                                .userId(userId)
                                .build()
                ).replyToken("replyToken")
                .postbackContent(
                        PostbackContent.builder()
                                .data(data)
                                .params(new HashMap<>())
                                .build()
                )
                .build();
    }

    private void prepareTextMessageEvent() throws ExecutionException, InterruptedException {
        given(lineMessagingClient.replyMessage(any(ReplyMessage.class))).willReturn(botApiCF);
        given(botApiCF.get()).willReturn(
                new BotApiResponse("requestId", "message", details)
        );

    }

    private void preparePostbackMessageEvent() throws ExecutionException, InterruptedException {
        given(stateService.getUserStateByIdLine(any()))
                .willReturn(state);
        given(lineMessagingClient.replyMessage(any(ReplyMessage.class))).willReturn(botApiCF);
        given(botApiCF.get()).willReturn(
                new BotApiResponse("requestId", "message", details)
        );
    }


    @Test
    void testTextEventShouldReturnHelp() throws ExecutionException, InterruptedException {
        prepareTextMessageEvent();
        State state = new BelumOrderState();
        given(stateService.getUserStateByIdLine(idLine))
                .willReturn(state);

        MessageEvent<TextMessageContent> messageEvent = createDummyMessageEvent("A Random Text", idLine);

        lineMessageController.handleTextEvent(messageEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
    }

    @Test
    void testPostbackEventHelp() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"help\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).help(idLine);
    }

    @Test
    void testPostbackEventOrder() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"order\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).pesan(idLine);
    }

    @Test
    void testPostbackEventCekHistory() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"cekHistory\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).cekHistory(idLine);
    }

    @Test
    void testPostbackEventCekProfil() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"cekProfil\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).cekProfil(idLine);
    }

    @Test
    void testPostbackEventUsePromo() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"usePromo\", \"name\": \"PromoLebaran\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).gunakanPromo(idLine, "PromoLebaran");
    }

    @Test
    void testPostbackEventCekKeranjang() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"cekKeranjang\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).cekKeranjang(idLine);
    }

    @Test
    void testPostbackEventTambahPesanan() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"tambahPesanan\", \"name\": \"Ayam Goreng\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).tambahPesanan(idLine, "Ayam Goreng");
    }

    @Test
    void testPostbackEventHapusPesanan() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"hapusPesanan\", \"name\": \"Ayam Goreng\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).hapusPesanan(idLine, "Ayam Goreng");
    }

    @Test
    void testPostbackEventSendKonfirmasi() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"sendKonfirmasi\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).sendKonfirmasi(idLine);
    }

    @Test
    void testPostbackEventKonfirmasi() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"konfirmasi\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).konfirmasi(idLine);
    }

    @Test
    void testPostbackEventOrderDone() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"orderDone\", \"idLine\": \"abc123\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).konfirmasi("abc123");
        verify(messageService, times(1)).pushMessage(any(), anyString());
    }

    @Test
    void testPostbackEventRandomEventShouldDoHelp() {
        when(stateService.getUserStateByIdLine(any())).thenReturn(stateMock);

        PostbackEvent postbackEvent = createDummyPostbackEvent(
                "{\"command\": \"random\"}",
                idLine
        );

        lineMessageController.handlePostbackEvent(postbackEvent);
        verify(lineMessagingClient, times(1)).replyMessage(any());
        verify(stateMock, times(1)).help(idLine);
    }

}
