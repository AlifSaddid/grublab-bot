package com.grublab.bot.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class CustomerTest {

    @InjectMocks
    private Customer customer;

    @BeforeEach
    void setUp() {
        customer.setPesanan(new ArrayList<>());
        customer.setDisplayName("Alif Saddid");
        customer.setIdLine("alifsaddid15");
        customer.setPoint(0);
        customer = new Customer("alifsaddid15", 0, new ArrayList<>(), "Alif Saddid");
    }

    @Test
    void testGetDisplayNameShouldReturnCorrectName() {
        assertEquals("Alif Saddid", customer.getDisplayName());
    }

    @Test
    void testGetPointShouldReturnCorrectPoint() {
        assertEquals(0, customer.getPoint());
    }

    @Test
    void testGetIdLineShouldReturnCorrectId() {
        assertEquals("alifsaddid15", customer.getIdLine());
    }

    @Test
    void testGetPesananShouldReturnList() {
        assertTrue(customer.getPesanan() instanceof List);
    }

}
