package com.grublab.bot.promo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PromoLebaranTest {

    @Test
    void testCalculateforPromoLebaran(){
        Promo promo = new PromoLebaran();
        assertEquals(9000, promo.calculate(10000));
    }
}
