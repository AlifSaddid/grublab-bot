package com.grublab.bot.promo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PromoTest {

    @Test
    void testPromoGetCostShouldReturnCost(){
        Promo promo = new PromoLebaran();
        promo.setCost(150);
        assertEquals(150, promo.getCost());

    }


}
