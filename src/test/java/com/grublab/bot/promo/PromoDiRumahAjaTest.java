package com.grublab.bot.promo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PromoDiRumahAjaTest {
    @Test
    void testCalculateforPromoDiRumahAja(){
        Promo promo = new PromoDiRumahAja();
        assertEquals(5000, promo.calculate(10000));
    }
}
