package com.grublab.bot.promo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PromoRamadanTest {
    private Promo promo;

    @BeforeEach
    public void setUp(){
        promo = new PromoRamadan();
    }
    @Test
    void testCalculateMethodReturnCorrectPrice(){
        assertEquals(20000-(20000*0.35), promo.calculate(20000));
    }
}
