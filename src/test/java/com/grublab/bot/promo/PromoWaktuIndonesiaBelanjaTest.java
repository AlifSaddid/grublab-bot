package com.grublab.bot.promo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PromoWaktuIndonesiaBelanjaTest {
    private Promo promo;

    @BeforeEach
    public void setUp(){
        promo = new PromoWaktuIndonesiaBelanja();
    }
    @Test
    void testCalculateMethodReturnCorrectPrice(){
        assertEquals(20000-10000, promo.calculate(20000));
        assertEquals(0, promo.calculate(1000));
    }
}
