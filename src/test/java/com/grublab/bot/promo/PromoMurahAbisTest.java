package com.grublab.bot.promo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PromoMurahAbisTest {

    private Promo promo;

    @BeforeEach
    public void setUp(){
        promo = new PromoMurahAbis();
    }
    @Test
    void testCalculateMethodReturnCorrectPrice(){
        assertEquals(20000-7000, promo.calculate(20000));
        assertEquals(0, promo.calculate(2000));
    }
}
