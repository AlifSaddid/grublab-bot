package com.grublab.bot.promo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PromoPSBBTest {
    private Promo promo;

    @BeforeEach
    public void setUp(){
        promo = new PromoPSBB();
    }
    @Test
    void testCalculateMethodReturnCorrectPrice(){
        assertEquals(20000-(20000*0.2), promo.calculate(20000));
    }
}
