package com.grublab.bot.services;

import com.grublab.bot.models.Customer;
import com.linecorp.bot.client.LineMessagingClient;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.http.HttpClient;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private LineMessagingClient lineMessagingClient;

    private String idLine;

    private Customer customer;

    @BeforeEach
    void setUp() {
        idLine = "ewa8e98123";

        userService.setRestTemplate(restTemplate);
        userService.setLineMessagingClient(lineMessagingClient);
        userService.setApi("api");

        customer = new Customer();
        customer.setIdLine(idLine);
    }

    @Test
    void testGetOrCreateCustomerShouldGetExistingCustomer() throws Exception {
        when(restTemplate.getForObject("api/v1/customers/" + idLine, Customer.class)).thenReturn(customer);
        Customer customer = userService.getOrCreateCustomerByIdLine(idLine);
        assertNotNull(customer);
        assertEquals(idLine, customer.getIdLine());
    }

    @Test
    void testGetOrCreateCustomerShouldReturnNewCustomer() {
        when(restTemplate.getForObject("api/v1/customers/" + idLine, Customer.class)).thenThrow(HttpClientErrorException.class);
        when(restTemplate.postForObject(any(String.class), any(HttpEntity.class), eq(Customer.class))).thenReturn(customer);
        Customer customer = userService.getOrCreateCustomerByIdLine(idLine);
        assertNotNull(customer);
        assertEquals(idLine, customer.getIdLine());
    }


}
