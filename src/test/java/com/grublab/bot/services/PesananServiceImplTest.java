package com.grublab.bot.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.models.PesananHistory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PesananServiceImplTest {

    @InjectMocks
    private PesananServiceImpl pesananService;

    @Mock
    private RestTemplate restTemplate;

    private ArrayList<PesananHistory> pesananHistories;

    private PesananHistory pesananHistory;

    private Menu menu;

    private String idLine;

    private Pesanan pesanan;

    @BeforeEach
    void setUp() {
        menu = new Menu("Ayam Goreng", "Ayam Digoreng", 10000);

        idLine = "wkwkwk";
        pesanan = new Pesanan(idLine);
        pesanan.addMenu(new Menu("Ayam Goreng", "Ayam Digoreng", 10000));
        pesanan.addMenu(new Menu("Ayam Goreng", "Ayam Digoreng", 10000));

        pesananService.setRestTemplate(restTemplate);
        pesananService.setApi("api");

        pesananHistory = new PesananHistory();
        pesananHistory.setMenus(new ArrayList<>());
        pesananHistory.getMenus().add(new Menu("Ayam Goreng", "Ayam Digoreng", 10000));

        pesananHistories = new ArrayList<>();
        pesananHistories.add(pesananHistory);
    }

    @Test
    void testGetCustomerOrderHistoryShouldReturnHistory() {
        when(restTemplate.getForObject("api/v1/orders/123", ArrayList.class)).thenReturn(pesananHistories);
        List<PesananHistory> result = pesananService.getCustomerOrderHistory("123");
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    void testCreatePesananBodyShouldReturnString() {
        String result = pesananService.createPesananBody(pesanan, idLine);

        assertNotNull(result);
        assertTrue(result instanceof String);
        assertTrue(result.contains("name"));
        assertTrue(result.contains("Ayam Goreng"));
    }
}
