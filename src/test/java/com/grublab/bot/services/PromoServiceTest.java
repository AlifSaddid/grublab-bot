package com.grublab.bot.services;

import com.grublab.bot.promo.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class PromoServiceTest {

    @InjectMocks
    private PromoServiceImpl promoService;

    @Test
    void testGetPromoByNameReturnNoPromo() {
        Promo promo = promoService.getPromoByName("Tidak Menggunakan Promo");
        assertTrue(promo instanceof NoPromo);
    }

    @Test
    void testGetPromoByNameReturnPromoDiRumahAja() {
        Promo promo = promoService.getPromoByName("Promo Di Rumah Aja");
        assertTrue(promo instanceof PromoDiRumahAja);
    }

    @Test
    void testGetPromoByNameReturnPromoLebaran() {
        Promo promo = promoService.getPromoByName("Promo Lebaran");
        assertTrue(promo instanceof PromoLebaran);
    }

    @Test
    void testGetPromoByNameReturnPromoMurahAbis() {
        Promo promo = promoService.getPromoByName("Promo Murah Abis");
        assertTrue(promo instanceof PromoMurahAbis);
    }

    @Test
    void testGetPromoByNameReturnPromoPSBB() {
        Promo promo = promoService.getPromoByName("Promo PSBB");
        assertTrue(promo instanceof PromoPSBB);
    }

    @Test
    void testGetPromoByNameReturnPromoRamadan() {
        Promo promo = promoService.getPromoByName("Promo Ramadan");
        assertTrue(promo instanceof PromoRamadan);
    }

    @Test
    void testGetPromoByNameReturnPromoWIB() {
        Promo promo = promoService.getPromoByName("Promo Waktu Indonesia Belanja");
        assertTrue(promo instanceof PromoWaktuIndonesiaBelanja);
    }

    @Test
    void testGetPromoByNameReturnNull() {
        Promo promo = promoService.getPromoByName("Not A Promo");
        assertNull(promo);
    }

}
