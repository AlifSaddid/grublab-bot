package com.grublab.bot.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grublab.bot.models.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MenuServiceImplTest {

    @InjectMocks
    private MenuServiceImpl menuService;

    @Mock
    private RestTemplate restTemplate;

    private String api;

    private ObjectMapper objectMapper;

    private ArrayList<Menu> menus;

    private Menu ayam;

    private Menu bebek;

    @BeforeEach
    public void setUp() {
        menuService.setRestTemplate(restTemplate);
        menuService.setApi("api");

        this.objectMapper = new ObjectMapper();

        ayam = new Menu();
        ayam.setName("Ayam Goreng");
        bebek = new Menu();
        bebek.setName("Bebek Goreng");
        menus = new ArrayList<>();
        menus.add(ayam);
        menus.add(bebek);
    }

    @Test
    void testGetMenusShouldReturnAllMenus() {
        when(restTemplate.getForObject("api/v1/menus/", ArrayList.class)).thenReturn(menus);
        List<Menu> result = menuService.getMenus();
        assertEquals(2, result.size());
        assertEquals("Ayam Goreng", result.get(0).getName());
        assertEquals("Bebek Goreng", result.get(1).getName());
    }

    @Test
    void testGetMenuByNameShouldReturnMenu() {
        when(restTemplate.getForObject("api/v1/menus/" + ayam.getName(), Menu.class)).thenReturn(ayam);
        Menu menu = menuService.getMenuByName("Ayam Goreng");
        assertNotNull(menu);
        assertEquals("Ayam Goreng", menu.getName());
    }

}
