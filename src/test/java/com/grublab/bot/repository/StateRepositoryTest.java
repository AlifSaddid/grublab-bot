package com.grublab.bot.repository;

import com.grublab.bot.states.BelumOrderState;

import com.grublab.bot.states.State;
import com.linecorp.bot.model.message.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

@Repository
class StateRepositoryTest {

    @Mock
    private StateRepository stateRepository;

    private State sampleState;
    private String idLine;

    @BeforeEach
    void setUp() {
        stateRepository = new StateRepository();
        sampleState = new BelumOrderState();
        idLine = "sampleID";
    }

    @Test
    void testGetOrCreateStateSuccessfullyCreatesAState() {
        State savedState = stateRepository.getUserStateOrCreate(idLine);

        List<Message> response = savedState.help(idLine);
        assertNotNull(response);
        assertEquals(1, response.size());
    }

    @Test
    void testGetOrCreateStateWithIdLineReturnsCurrentState() {
        State setState = sampleState;
        stateRepository.setStateByIdLine(idLine, setState);

        State savedState = stateRepository.getUserStateOrCreate(idLine);
        assertThat(savedState).isEqualTo(setState);
    }

}

