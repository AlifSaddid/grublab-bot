package com.grublab.bot;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class BotApplicationTests {

    @Test
    void contextLoads() {
        assertTrue(true);
    }

    @Test
    void testMain() {
        BotApplication.main(new String[] {});
        assertTrue(true);
    }

}
