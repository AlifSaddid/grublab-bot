package com.grublab.bot.promo;

public abstract class Promo {
    private String description;
    private int cost;
    private String name;

    public abstract int calculate(int price);

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public int getCost() {
        return this.cost;
    }

    public String getName() {
        return this.name;
    }
}
