package com.grublab.bot.promo;

public class PromoPSBB extends Promo{

    public PromoPSBB() {
        setDescription("Diskon 20% biar PSBB mu nggak bosenin!");
        setCost(110);
        setName("Promo PSBB");
    }

    @Override
    public int calculate(int price) {
        return (int)(price - (price * 0.2));
    }
}
