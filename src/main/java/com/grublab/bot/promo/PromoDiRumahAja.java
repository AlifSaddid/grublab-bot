package com.grublab.bot.promo;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class PromoDiRumahAja extends Promo{

    public PromoDiRumahAja() {
        setDescription("Promo pengurangan harga Rp5000 khusus untuk kamu yang di rumah aja!");
        setCost(100);
        setName("Promo Di Rumah Aja");
    }

    @Override
    public int calculate(int price) {
        return max(0, price - 5000);
    }
}
