package com.grublab.bot.promo;

import static java.lang.Integer.max;

public class PromoWaktuIndonesiaBelanja extends Promo{

    public PromoWaktuIndonesiaBelanja() {
        setDescription("Waktu Indonesia Belanja di Grublab! Potongan harga hingga Rp10.000!");
        setCost(130);
        setName("Promo Waktu Indonesia Belanja");
    }

    @Override
    public int calculate(int price) {
        return max(0, price - 10000);
    }
}
