package com.grublab.bot.promo;

public class PromoRamadan extends Promo{

    public PromoRamadan() {
        setDescription("Ramadan berkah! Diskon 35% untuk semua makanan dan minuman yang kamu suka!");
        setCost(150);
        setName("Promo Ramadan");
    }

    @Override
    public int calculate(int price) {
        return (int)(price - (price * 0.35));
    }
}
