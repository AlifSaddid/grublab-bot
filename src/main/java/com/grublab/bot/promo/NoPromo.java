package com.grublab.bot.promo;

public class NoPromo extends Promo {

    public NoPromo() {
        setDescription("Anda tidak menggunakan promo");
        setCost(0);
        setName("Tidak Menggunakan Promo");
    }

    @Override
    public int calculate(int price) {
        return price;
    }
}
