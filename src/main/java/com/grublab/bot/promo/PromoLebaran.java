package com.grublab.bot.promo;

public class PromoLebaran extends Promo{

    public PromoLebaran() {
        setDescription("Promo meriah diskon 10% untuk menyambut hari raya!");
        setCost(150);
        setName("Promo Lebaran");
    }

    @Override
    public int calculate(int price) {
        return (int)(price - (price * 0.1));
    }
}
