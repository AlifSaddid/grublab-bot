package com.grublab.bot.promo;

import static java.lang.Integer.max;

public class PromoMurahAbis extends Promo{

    public PromoMurahAbis() {
        setDescription("Murah abis! Diskon maksimal Rp7000!");
        setCost(120);
        setName("Promo Murah Abis");
    }

    @Override
    public int calculate(int price) {
        return max(0, price - 7000);
    }
}
