package com.grublab.bot.messages;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.Arrays;
import java.util.function.Supplier;

public class KonfirmasiMessageSupplier implements Supplier<Message> {
    private String text;

    public KonfirmasiMessageSupplier(String text) {
        this.text = text;
    }

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Konfirmasi!", bubble);
    }

    public Box createBody() {
        Text textComponent = Text.builder()
                .text(this.text)
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();

        PostbackAction sendKonfirmasiAction = PostbackAction.builder()
                .label("Konfirmasi")
                .data("{\"command\": \"konfirmasi\"}")
                .displayText("Ya, saya yakin!")
                .build();

        Button sendKonfirmasiButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(sendKonfirmasiAction)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(textComponent, sendKonfirmasiButton))
                .spacing(FlexMarginSize.MD)
                .build();
    }

}
