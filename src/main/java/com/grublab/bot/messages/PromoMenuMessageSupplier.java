package com.grublab.bot.messages;

import com.grublab.bot.promo.Promo;
import com.grublab.bot.services.PromoService;
import com.grublab.bot.services.PromoServiceImpl;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class PromoMenuMessageSupplier implements Supplier<Message> {

    private PromoService promoService = new PromoServiceImpl();

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Berikut promo yang tersedia!", bubble);
    }

    private Box createBody() {
        Text text = Text.builder()
                .text("Berikut promo yang tersedia!")
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();
        Box upper = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(text)
                .build();

        List<Promo> promos = promoService.getPromos();
        List<FlexComponent> promoBoxes = new ArrayList<>();

        for (Promo promo : promos) {
            promoBoxes.add(createPromoBox(promo));
        }

        PostbackAction action = PostbackAction.builder()
                .data(String.format("{\"command\": \"%s\"}", "cekKeranjang"))
                .label("Cek Keranjang")
                .displayText("Aku mau cek keranjang belanjaan dong!")
                .build();

        Button button = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(action)
                .build();

        PostbackAction batalPromoAction = PostbackAction.builder()
                .data(String.format("{\"command\": \"%s\"}", "hapusPromo"))
                .label("Batalkan Promo")
                .displayText(("Batalkan menggunakan promo dong!"))
                .build();

        Button batalPromoButton = button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(batalPromoAction)
                .build();

        Box lower = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(promoBoxes)
                .spacing(FlexMarginSize.MD)
                .build();

        PostbackAction backAction = PostbackAction.builder()
                .data(String.format("{\"command\": \"%s\"}", "backConfirmation"))
                .label("Kembali")
                .displayText("Kembali ke proses sebelumnya!")
                .build();

        Button backButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(backAction)
                .build();

        PostbackAction cancelConfirmationAction = PostbackAction.builder()
                .data(String.format("{\"command\": \"%s\"}", "cancelConfirmation"))
                .label("Batal")
                .displayText("Batalkan pembelian!")
                .build();

        Button cancelConfirmationButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(cancelConfirmationAction)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(upper, lower, button, batalPromoButton, backButton, cancelConfirmationButton))
                .spacing(FlexMarginSize.MD)
                .build();
    }

    private Box createPromoBox(Promo promo) {
        Text name = Text.builder()
                .text(promo.getName())
                .size(FlexFontSize.XL)
                .weight(Text.TextWeight.BOLD)
                .wrap(true)
                .build();

        Text description = Text.builder()
                .text(promo.getDescription())
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();
        
        Text price = Text.builder()
                .text("Cost: " + promo.getCost() + " points")
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        PostbackAction action = PostbackAction.builder()
                .label("+")
                .data(String.format("{\"command\": \"%s\", \"name\": \"%s\"}", "usePromo", promo.getName()))
                .displayText(String.format("Saya mau menggunakan promo %s!", promo.getName()))
                .build();

        Button button = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(action)
                .build();

        Box left = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(name, description, price))
                .flex(4)
                .build();

        Box right = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(button))
                .flex(1)
                .build();

        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Arrays.asList(left, right))
                .build();
    }

}