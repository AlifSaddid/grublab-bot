package com.grublab.bot.messages;

import com.grublab.bot.models.Customer;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.Arrays;
import java.util.function.Supplier;

public class UserProfileMessageSupplier implements Supplier<Message> {

    private Customer customer;

    public UserProfileMessageSupplier(Customer customer) {
        this.customer = customer;
    }

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Profile Anda", bubble);
    }

    private Box createBody() {
        Text text = Text.builder()
                .text("Profile Anda")
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();
        Box upper = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(text)
                .build();

        Box userProfile = createHistoryBox(customer.getDisplayName(), customer.getPoint());

        Box lower = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(userProfile))
                .spacing(FlexMarginSize.MD)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(upper, lower))
                .spacing(FlexMarginSize.MD)
                .build();
    }

    private Box createHistoryBox(String name, int points) {
        Text nama = Text.builder()
                .text(name)
                .size(FlexFontSize.XL)
                .weight(Text.TextWeight.BOLD)
                .wrap(true)
                .build();


        Text poin = Text.builder()
                .text(String.format("Point: %s", String.valueOf(points)))
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        Box profile = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(nama, poin))
                .flex(4)
                .build();

        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Arrays.asList(profile))
                .build();
    }
}