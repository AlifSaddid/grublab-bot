package com.grublab.bot.messages;

import com.grublab.bot.models.Customer;
import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class SellerConfirmationMessageSupplier implements Supplier<Message> {
    private Pesanan pesanan;

    private Customer customer;

    public SellerConfirmationMessageSupplier(Pesanan pesanan, Customer customer) {
        this.pesanan = pesanan;
        this.customer = customer;
    }

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Ada pesanan baru nih!", bubble);
    }

    public Box createBody() {
        Text text = Text.builder()
                .text("Pesanan dari " + customer.getDisplayName())
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();
        Box upper = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(text)
                .build();

        List<FlexComponent> menuBoxes = new ArrayList<>();
        for (Menu menu : this.pesanan.getOrderList()) {
            menuBoxes.add(createMenuBox(menu));
        }

        Box lower = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(menuBoxes)
                .spacing(FlexMarginSize.MD)
                .build();

        PostbackAction orderDoneAction = PostbackAction.builder()
                .label("Pesanan Selesai")
                .data(String.format("{\"command\": \"%s\", \"idLine\": \"%s\"}", "orderDone", pesanan.getIdLine()))
                .displayText(String.format("Pesanan milik %s sudah selesai!", customer.getDisplayName()))
                .build();

        Button orderDoneButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(orderDoneAction)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(upper, lower, orderDoneButton))
                .spacing(FlexMarginSize.MD)
                .build();
    }

    private Box createMenuBox(Menu menu) {
        Text name = Text.builder()
                .text(menu.getName())
                .size(FlexFontSize.XL)
                .weight(Text.TextWeight.BOLD)
                .wrap(true)
                .build();

        Text description = Text.builder()
                .text(menu.getDescription())
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        Text price = Text.builder()
                .text(String.valueOf(menu.getPrice()))
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        Box container = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(name, description, price))
                .flex(4)
                .build();

        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .content(container)
                .build();
    }
}
