package com.grublab.bot.messages;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.Arrays;
import java.util.function.Supplier;

public class KonfirmasiSenderMessageSupplier implements Supplier<Message> {

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Tekan untuk konfirmasi", bubble);
    }

    public Box createBody() {
        Text text = Text.builder()
                .text("Silakan tekan tombol di bawah ini untuk mengkonfirmasi pesanan anda")
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();

        PostbackAction sendKonfirmasiAction = PostbackAction.builder()
                .label("Konfirmasi")
                .data("{\"command\": \"sendKonfirmasi\"}")
                .label("Saya mau konfirmasi!")
                .build();

        Button sendKonfirmasiButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(sendKonfirmasiAction)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(text, sendKonfirmasiButton))
                .spacing(FlexMarginSize.MD)
                .build();
    }

}
