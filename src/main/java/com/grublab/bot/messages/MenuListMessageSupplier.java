package com.grublab.bot.messages;

import com.grublab.bot.models.Menu;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class MenuListMessageSupplier implements Supplier<Message> {
    private List<Menu> menus;

    public MenuListMessageSupplier(List<Menu> menus) {
        this.menus = menus;
    }

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Berikut menu yang tersedia!", bubble);
    }

    private Box createBody() {
        Text text = Text.builder()
                .text("Berikut menu yang tersedia!")
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();
        Box upper = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(text)
                .build();

        List<FlexComponent> menuBoxes = new ArrayList<>();
        for (Menu menu : menus) {
            menuBoxes.add(createMenuBox(menu));
        }

        Box lower = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(menuBoxes)
                .spacing(FlexMarginSize.MD)
                .build();

        PostbackAction keranjangAction = PostbackAction.builder()
                .data(String.format("{\"command\": \"%s\"}", "cekKeranjang"))
                .label("Cek Keranjang")
                .displayText("Aku mau cek keranjang belanjaan dong!")
                .build();

        Button keranjangButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(keranjangAction)
                .build();

        PostbackAction backAction = PostbackAction.builder()
                .data(String.format("{\"command\": \"%s\"}", "backConfirmation"))
                .label("Kembali")
                .displayText("Kembali ke proses sebelumnya!")
                .build();

        Button backButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(backAction)
                .build();

        PostbackAction cancelConfirmationAction = PostbackAction.builder()
                .data(String.format("{\"command\": \"%s\"}", "cancelConfirmation"))
                .label("Batal")
                .displayText("Batalkan pembelian!")
                .build();

        Button cancelConfirmationButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(cancelConfirmationAction)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(upper, lower, keranjangButton, backButton, cancelConfirmationButton))
                .spacing(FlexMarginSize.MD)
                .build();
    }

    private Box createMenuBox(Menu menu) {
        Text name = Text.builder()
                .text(menu.getName())
                .size(FlexFontSize.XL)
                .weight(Text.TextWeight.BOLD)
                .wrap(true)
                .build();

        Text description = Text.builder()
                .text(menu.getDescription())
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        Text price = Text.builder()
                .text(String.valueOf(menu.getPrice()))
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        PostbackAction action = PostbackAction.builder()
                .label("+")
                .data(String.format("{\"command\": \"%s\", \"name\": \"%s\"}", "tambahPesanan", menu.getName()))
                .displayText(String.format("Saya mau pesan %s!", menu.getName()))
                .build();

        Button button = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(action)
                .build();

        Box left = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(name, description, price))
                .flex(4)
                .build();

        Box right = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(button))
                .flex(1)
                .build();

        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Arrays.asList(left, right))
                .build();
    }

}
