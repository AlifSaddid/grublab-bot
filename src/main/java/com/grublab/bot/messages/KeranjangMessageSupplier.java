package com.grublab.bot.messages;

import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.promo.Promo;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class KeranjangMessageSupplier implements Supplier<Message> {
    private Pesanan pesanan;

    public KeranjangMessageSupplier(Pesanan pesanan) {
        this.pesanan = pesanan;
    }

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Berikut keranjang anda!", bubble);
    }

    public Box createBody() {
        Text text = Text.builder()
                .text("Berikut isi keranjang kamu!")
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();
        Box upper = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(text)
                .build();

        List<FlexComponent> menuBoxes = new ArrayList<>();
        for (Menu menu : this.pesanan.getOrderList()) {
            menuBoxes.add(createMenuBox(menu));
        }

        Box lower = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(menuBoxes)
                .spacing(FlexMarginSize.MD)
                .build();

        Box price = createPriceBox("Total harga", pesanan.getPrice());

        Box promo = createPromoBox(pesanan.getPromo());

        Box priceAfterPromo = createPriceBox("Total harga setelah promo", pesanan.getPriceWithPromo());

        Box keterangan = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(price, promo, priceAfterPromo)
                .spacing(FlexMarginSize.MD)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(upper, lower, keterangan))
                .spacing(FlexMarginSize.MD)
                .build();
    }

    private Box createPriceBox(String keterangan, int price) {
        Text priceText = Text.builder()
                .text(keterangan + ": Rp" + price)
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .content(priceText)
                .build();
    }

    private Box createPromoBox(Promo promo) {
        Text priceText = Text.builder()
                .text("Promo dipilih: " + promo.getName())
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .content(priceText)
                .build();
    }

    private Box createMenuBox(Menu menu) {
        Text name = Text.builder()
                .text(menu.getName())
                .size(FlexFontSize.XL)
                .weight(Text.TextWeight.BOLD)
                .wrap(true)
                .build();

        Text description = Text.builder()
                .text(menu.getDescription())
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        Text price = Text.builder()
                .text("Rp" + menu.getPrice())
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        PostbackAction action = PostbackAction.builder()
                .label("-")
                .data(String.format("{\"command\": \"%s\", \"name\": \"%s\"}", "hapusPesanan", menu.getName()))
                .displayText(String.format("Saya batal pesan %s!", menu.getName()))
                .build();

        Button button = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(action)
                .build();

        Box left = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(name, description, price))
                .flex(4)
                .build();

        Box right = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(button))
                .flex(1)
                .build();

        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Arrays.asList(left, right))
                .build();
    }
}
