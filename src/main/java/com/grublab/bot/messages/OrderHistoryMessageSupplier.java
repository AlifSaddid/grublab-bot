package com.grublab.bot.messages;

import com.grublab.bot.models.Menu;
import com.grublab.bot.models.PesananHistory;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class OrderHistoryMessageSupplier implements Supplier<Message> {

    private List<PesananHistory> pesananHistory;

    public OrderHistoryMessageSupplier(List<PesananHistory> pesananHistory) {
        this.pesananHistory = pesananHistory;
    }

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Berikut adalah history pemesanan Anda:", bubble);
    }

    private Box createBody() {
        Text text = Text.builder()
                .text("Berikut adalah history pemesanan Anda:")
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();
        Box upper = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(text)
                .build();

        List<FlexComponent> histories = new ArrayList<>();
        for (PesananHistory pesanan : this.pesananHistory) {
            histories.add(createPesananBox(pesanan));
        }


        Box lower = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(histories)
                .spacing(FlexMarginSize.MD)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(upper, lower))
                .spacing(FlexMarginSize.MD)
                .build();
    }

    private Box createPesananBox(PesananHistory pesananHistory) {
        FlexComponent date = Text.builder()
                .text(pesananHistory.getDate())
                .size(FlexFontSize.LG)
                .weight(Text.TextWeight.BOLD)
                .wrap(true)
                .build();

        List<FlexComponent> menuBoxes = new ArrayList<>();
        for (Menu menu : pesananHistory.getMenus()) {
            menuBoxes.add(createMenuBox(menu));
        }

        FlexComponent boxMenu = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(menuBoxes)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(date, boxMenu))
                .build();
    }

    private Box createMenuBox(Menu menu) {
        Text name = Text.builder()
                .text(menu.getName())
                .size(FlexFontSize.Md)
                .weight(Text.TextWeight.BOLD)
                .wrap(true)
                .build();

        Text price = Text.builder()
                .text(String.valueOf(menu.getPrice()))
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        Box left = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(name, price))
                .flex(4)
                .build();

        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Arrays.asList(left))
                .build();
    }
}
