package com.grublab.bot.messages;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.Arrays;
import java.util.function.Supplier;

public class InitialMenuMessageSupplier implements Supplier<Message> {

    public Message get() {
        Box body = createBody();

        Bubble bubble = Bubble.builder()
                .body(body)
                .build();

        return new FlexMessage("Selamat Datang di GrubLab!", bubble);
    }

    private Box createBody() {
        Text text = Text.builder()
                .text("Selamat datang di GrubLab Bot! Silakan klik tombol di bawah ini untuk mulai memesan")
                .align(FlexAlign.CENTER)
                .wrap(true)
                .build();

        Button orderButton = createButton(
                "order",
                "Hai GrubLab, saya mau pesan dong!",
                "Pesan"
        );
        Button historyButton = createButton(
                "cekHistory",
                "Hai GrubLab, saya mau lihat history pemesanan nih!",
                "Lihat History"
        );
        Button profilButton = createButton(
                "cekProfil",
                "Hai GrubLab, tolong tampilin informasi profil saya ya!",
                "Cek Profil"
        );

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(text, orderButton, historyButton, profilButton))
                .spacing(FlexMarginSize.MD)
                .build();
    }

    private Button createButton(String command, String displayText, String label) {
        PostbackAction action = PostbackAction.builder()
                .label(label)
                .data(String.format("{\"command\": \"%s\"}", command))
                .displayText(displayText)
                .build();

        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#00AA13")
                .gravity(FlexGravity.CENTER)
                .action(action)
                .build();
    }

}
