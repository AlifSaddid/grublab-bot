package com.grublab.bot.repository;

import com.grublab.bot.states.BelumOrderState;
import com.grublab.bot.states.State;

import java.util.HashMap;

public class StateRepository {
    private static HashMap<String, State> userStates = new HashMap<>();

    public static State getUserStateOrCreate(String idLine) {
        if (userStates.get(idLine) == null) userStates.put(idLine, new BelumOrderState());
        return userStates.get(idLine);
    }

    public static void setStateByIdLine(String idLine, State state) {
        userStates.put(idLine, state);
    }
}
