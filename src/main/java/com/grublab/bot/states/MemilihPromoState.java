package com.grublab.bot.states;

import com.grublab.bot.messages.*;
import com.grublab.bot.models.Customer;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.promo.NoPromo;
import com.grublab.bot.promo.Promo;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.Arrays;
import java.util.List;

public class MemilihPromoState extends State {

    public MemilihPromoState(Pesanan pesanan) {
        super(pesanan);
    }

    @Override
    public List<Message> help(String idLine) {
        this.clearResponses();
        this.addResponse(new PromoMenuMessageSupplier().get());
        this.addResponse(new KonfirmasiSenderMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> pesan(String idLine) {
        return Arrays.asList(new TextMessage(
                "Silahkan memilih promo"
        ));
    }

    @Override
    public List<Message> tambahPesanan(String idLine, String menu) {
        return Arrays.asList(new TextMessage(
                "Anda sudah melakukan pemesanan."
        ));
    }

    @Override
    public List<Message> hapusPesanan(String idLine, String menu) {
        return Arrays.asList(new TextMessage(
                "Anda sudah melakukan pemesanan."
        ));
    }

    @Override
    public List<Message> sendKonfirmasi(String idLine) {
        this.clearResponses();
        this.addResponse(new KeranjangMessageSupplier(this.pesanan).get());
        this.addResponse(new KonfirmasiMessageSupplier("Kamu yakin dengan pilihan promonya?").get());
        return this.getResponses();
    }

    @Override
    public List<Message> konfirmasi(String idLine) {
        stateService.setUserState(idLine, new OrderSedangDibuatState(this.pesanan));
        Customer customer = userService.getOrCreateCustomerByIdLine(idLine);
        messageService.sendMessageToSeller(new SellerConfirmationMessageSupplier(this.pesanan, customer).get());
        return Arrays.asList(new TextMessage(
           "Terima kasih. Pesanan anda akan segera dibuat, mohon ditunggu!"
        ));
    }

    @Override
    public List<Message> batal(String idLine) {
        stateService.setUserState(idLine, new BelumOrderState());
        this.clearResponses();
        addResponse(new TextMessage("Pesanan anda telah dibatalkan, anda sedang tidak memiliki pesanan."));
        addResponse(new InitialMenuMessageSupplier().get());
        return getResponses();
    }

    @Override
    public List<Message> kembali(String idLine) {
        stateService.setUserState(idLine, new SedangOrderState(this.pesanan));
        this.clearResponses();
        addResponse(new TextMessage("Anda kembali ke proses sebelumnya, silakan memilih menu!"));
        addResponse(new MenuListMessageSupplier(menuService.getMenus()).get());
        addResponse(new KonfirmasiSenderMessageSupplier().get());
        return getResponses();
    }

    @Override
    public List<Message> batalConfirmation(String idLine) {
        return Arrays.asList(
                new KonfirmasiBatalMessageSupplier().get()
        );
    }

    @Override
    public List<Message> kembaliConfirmation(String idLine) {
        return Arrays.asList(
                new KonfirmasiKembaliMessageSupplier().get()
        );
    }

    @Override
    public List<Message> cekPromo(String idLine) {
        return Arrays.asList(new PromoMenuMessageSupplier().get());
    }

    @Override
    public List<Message> gunakanPromo(String idLine, String promoName) {
        Promo promo = promoService.getPromoByName(promoName);
        Customer customer = userService.getOrCreateCustomerByIdLine(idLine);
        if (customer.getPoint() < promo.getCost()) {
            return Arrays.asList(new TextMessage("Poin anda tidak cukup untuk menggunakan promo " + promo.getName()));
        }
        pesanan.setPromo(promo);
        return Arrays.asList(new TextMessage("Anda memilih untuk menggunakan menggunakan promo " + promo.getName()));
    }

    @Override
    public List<Message> hapusPromo(String idLine) {
        pesanan.setPromo(new NoPromo());
        return Arrays.asList(new TextMessage("Berhasil menghapus promo! Sekarang kamu tidak menggunakan promo."));
    }

    @Override
    public List<Message> cekKeranjang(String idLine) {
        return Arrays.asList(new KeranjangMessageSupplier(this.pesanan).get());
    }

}
