package com.grublab.bot.states;

import com.grublab.bot.messages.KeranjangMessageSupplier;
import com.grublab.bot.messages.KonfirmasiMessageSupplier;
import com.grublab.bot.models.Customer;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.promo.Promo;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.Arrays;
import java.util.List;

public class OrderSedangDiantarState extends State {

    public OrderSedangDiantarState(Pesanan pesanan) {
        super(pesanan);
    }

    @Override
    public List<Message> help(String idLine) {
        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> pesan(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> tambahPesanan(String idLine, String menu) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> hapusPesanan(String idLine, String menu) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> cekHistory(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> cekProfil(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> sendKonfirmasi(String idLine) {
        this.clearResponses();
        this.addResponse(new KeranjangMessageSupplier(this.pesanan).get());
        this.addResponse(new KonfirmasiMessageSupplier("Apakah driver kamu sudah sampai?").get());
        return this.getResponses();
    }

    @Override
    public List<Message> konfirmasi(String idLine) {
        pesananService.savePesanan(this.pesanan, idLine);
        stateService.setUserState(idLine, new BelumOrderState());
        Customer customer = userService.getOrCreateCustomerByIdLine(idLine);
        Promo promo = pesanan.getPromo();
        Integer newPoint = customer.getPoint() - promo.getCost() + pesanan.getPriceWithPromo() / 100;
        userService.updateCustomerPoint(idLine, newPoint);
        return Arrays.asList(new TextMessage(
                "Terima Kasih Sudah Memesan di Grublab!"
        ), new TextMessage(
                "Anda mendapatkan " + pesanan.getPriceWithPromo() / 100 + " points dari pembelian!"
        ), new TextMessage(
                "Point anda berkurang " + promo.getCost() + " karena menggunakan " + promo.getName()
        ));
    }

    @Override
    public List<Message> batal(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> kembali(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> batalConfirmation(String idLine) {
        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> kembaliConfirmation(String idLine) {
        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> cekPromo(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> gunakanPromo(String idLine, String promo) {
        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> hapusPromo(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

    @Override
    public List<Message> cekKeranjang(String idLine) {

        return Arrays.asList(new TextMessage(
                "Mohon tunggu pesanan Anda diantar oleh driver kami. Apabila pesanan sudah sampai, mohon mengkonfirmasi."
        ));
    }

}
