package com.grublab.bot.states;

import com.grublab.bot.messages.*;
import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class SedangOrderState extends State {

    public SedangOrderState(Pesanan pesanan) {
        super(pesanan);
    }

    @Override
    public List<Message> help(String idLine) {
        this.clearResponses();
        List<Menu> menu = menuService.getMenus();
        this.addResponse(new MenuListMessageSupplier(menu).get());
        this.addResponse(new KonfirmasiSenderMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> pesan(String idLine) { return Arrays.asList(new TextMessage("Anda sedang order.")); }

    @Override
    public List<Message> tambahPesanan(String idLine, String menu) {
        this.pesanan.addMenu(menuService.getMenuByName(menu));

        return Arrays.asList(new TextMessage(menu + " telah ditambahkan ke pesanan Anda."));
    }

    @Override
    public List<Message> hapusPesanan(String idLine, String menu) {
        this.pesanan.removeMenu(menu);

        return Arrays.asList(new TextMessage(menu + " telah dihapus dari pesanan Anda."));
    }

    @Override
    public List<Message> sendKonfirmasi(String idLine) {
        this.clearResponses();
        this.addResponse(new KeranjangMessageSupplier(this.pesanan).get());
        this.addResponse(new KonfirmasiMessageSupplier("Kamu yakin sama pesanannya?").get());
        return this.getResponses();
    }

    @Override
    public List<Message> konfirmasi(String idLine) {
        stateService.setUserState(idLine, new MemilihPromoState(this.pesanan));
        this.clearResponses();
        this.addResponse(new TextMessage("Silakan memilih promo!"));
        this.addResponse(new PromoMenuMessageSupplier().get());
        this.addResponse(new KonfirmasiSenderMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> batal(String idLine) {
        this.pesanan.cancelOrder();
        stateService.setUserState(idLine, new BelumOrderState());
        this.clearResponses();
        addResponse(new TextMessage("Pesanan anda telah dibatalkan, anda sedang tidak memiliki pesanan."));
        addResponse(new InitialMenuMessageSupplier().get());
        return getResponses();
    }

    @Override
    public List<Message> kembali(String idLine) {
        stateService.setUserState(idLine, new BelumOrderState());
        this.clearResponses();
        addResponse(new TextMessage("Pesanan anda telah dibatalkan, anda sedang tidak memiliki pesanan."));
        addResponse(new InitialMenuMessageSupplier().get());
        return getResponses();
    }

    @Override
    public List<Message> batalConfirmation(String idLine) {
        return Arrays.asList(
                new KonfirmasiBatalMessageSupplier().get()
        );
    }

    @Override
    public List<Message> kembaliConfirmation(String idLine) {
        return Arrays.asList(
                new KonfirmasiKembaliMessageSupplier().get()
        );
    }

    @Override
    public List<Message> cekPromo(String idLine) { return Arrays.asList(new TextMessage("Selesaikan pemesanan untuk melihat promo.")); }

    @Override
    public List<Message> gunakanPromo(String idLine, String promo) { return Arrays.asList(new TextMessage("Silakan pesan sebelum memilih promo")); }

    @Override
    public List<Message> hapusPromo(String idLine) { return Arrays.asList(new TextMessage("Silakan pesan sebelum memilih promo")); }

    @Override
    public List<Message> cekKeranjang(String idLine) { return Arrays.asList(new KeranjangMessageSupplier(this.pesanan).get()); }

}
