package com.grublab.bot.states;

import com.grublab.bot.messages.KonfirmasiSenderMessageSupplier;
import com.grublab.bot.models.Pesanan;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class OrderSedangDibuatState extends State {

    public OrderSedangDibuatState(Pesanan pesanan) {
        super(pesanan);
    }

    @Override
    public List<Message> help(String idLine) {
        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, Mohon menunggu"
        ));
    }

    @Override
    public List<Message> pesan(String idLine) {
        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, Mohon menunggu."
        ));
    }

    @Override
    public List<Message> tambahPesanan(String idLine, String menu) {

        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, Mohon menunggu."
        ));
    }

    @Override
    public List<Message> hapusPesanan(String idLine, String menu) {

        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, mohon menunggu."
        ));
    }

    @Override
    public List<Message> sendKonfirmasi(String idLine) {
        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, mohon menunggu."
        ));
    }

    @Override
    public List<Message> konfirmasi(String idLine) {
        stateService.setUserState(idLine, new OrderSedangDiantarState(this.pesanan));
        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        messageService.pushMessage(new KonfirmasiSenderMessageSupplier().get(), idLine);
                    }
                },
                5000
        );
        return Arrays.asList(new TextMessage(
                "Pesanan anda sudah selesai!"
        ));
    }

    @Override
    public List<Message> batal(String idLine) {

        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, mohon menunggu."
        ));    }

    @Override
    public List<Message> kembali(String idLine) {

        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, mohon menunggu."
        ));
    }

    @Override
    public List<Message> batalConfirmation(String idLine) {
        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, mohon menunggu."
        ));
    }

    @Override
    public List<Message> kembaliConfirmation(String idLine) {
        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, mohon menunggu."
        ));
    }

    @Override
    public List<Message> cekPromo(String idLine) {

        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, mohon menunggu."
        ));
    }

    @Override
    public List<Message> gunakanPromo(String idLine, String promo) {

        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, Mohon menunggu."
        ));
    }

    @Override
    public List<Message> hapusPromo(String idLine) {
        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, Mohon menunggu."
        ));
    }

    @Override
    public List<Message> cekKeranjang(String idLine) {

        return Arrays.asList(new TextMessage(
                "Pesanan sedang dibuat, Mohon menunggu."
        ));
    }

}
