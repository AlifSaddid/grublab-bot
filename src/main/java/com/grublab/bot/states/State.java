package com.grublab.bot.states;

import com.grublab.bot.messages.OrderHistoryMessageSupplier;
import com.grublab.bot.messages.UserProfileMessageSupplier;
import com.grublab.bot.models.Customer;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.services.*;
import com.linecorp.bot.model.message.Message;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Setter
public abstract class State {
    protected Pesanan pesanan;
    private List<Message> responses = new ArrayList<>();

    protected StateService stateService;

    protected MenuService menuService;

    protected PromoService promoService;

    protected MessageService messageService;

    protected UserService userService;

    protected PesananService pesananService;

    protected State(Pesanan pesanan) {
        this();
        this.pesanan = pesanan;
    }

    protected State() {
        setStateService(new StateServiceImpl());
        setMenuService(new MenuServiceImpl());
        setPromoService(new PromoServiceImpl());
        setMessageService(new MessageServiceImpl());
        setUserService(new UserServiceImpl());
        setPesananService(new PesananServiceImpl());
    }

    public abstract List<Message> help(String idLine);

    public abstract List<Message> pesan(String idLine);

    public abstract List<Message> tambahPesanan(String idLine, String menu);

    public abstract List<Message> hapusPesanan(String idLine, String menu);

    public List<Message> cekHistory(String idLine) {
        this.clearResponses();
        this.addResponse(new OrderHistoryMessageSupplier(pesananService.getCustomerOrderHistory(idLine)).get());
        return this.getResponses();
    }

    public List<Message> cekProfil(String idLine) {
        this.clearResponses();
        Customer customer = userService.getOrCreateCustomerByIdLine(idLine);
        this.addResponse(new UserProfileMessageSupplier(customer).get());
        return this.getResponses();
    }

    public abstract List<Message> sendKonfirmasi(String idLine);

    public abstract List<Message> konfirmasi(String idLine);

    public abstract List<Message> batal(String idLine);

    public abstract List<Message> batalConfirmation(String idLine);

    public abstract List<Message> kembali(String idLine);

    public abstract List<Message> kembaliConfirmation(String idLine);

    public abstract List<Message> cekPromo(String idLine);

    public abstract List<Message> gunakanPromo(String idLine, String promo);

    public abstract List<Message> hapusPromo(String idLine);

    public abstract List<Message> cekKeranjang(String idLine);

    public void addResponse(Message message) {
        this.responses.add(message);
    }

    public void clearResponses() {
        this.responses.clear();
    }

    public List<Message> getResponses() {
        return this.responses;
    }
}
