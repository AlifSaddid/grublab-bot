package com.grublab.bot.states;

import com.grublab.bot.messages.InitialMenuMessageSupplier;
import com.grublab.bot.messages.KonfirmasiSenderMessageSupplier;
import com.grublab.bot.messages.MenuListMessageSupplier;
import com.grublab.bot.models.Pesanan;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.Arrays;
import java.util.List;

public class BelumOrderState extends State {

    @Override
    public List<Message> help(String idLine) {
        this.clearResponses();
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> pesan(String idLine) {
        stateService.setUserState(idLine, new SedangOrderState(new Pesanan(idLine)));
        this.clearResponses();
        this.addResponse(new MenuListMessageSupplier(menuService.getMenus()).get());
        this.addResponse(new KonfirmasiSenderMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> tambahPesanan(String idLine, String menu) {
        this.clearResponses();
        this.addResponse(new TextMessage("Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> hapusPesanan(String idLine, String menu) {
        this.clearResponses();
        this.addResponse(new TextMessage("Anda sedang tidak memesan menu. Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> sendKonfirmasi(String idLine) {
        this.clearResponses();
        this.addResponse(new TextMessage("Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        this.addResponse(new KonfirmasiSenderMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> konfirmasi(String idLine) {
        this.clearResponses();
        this.addResponse(new TextMessage("Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> batal(String idLine) {
        this.clearResponses();
        this.addResponse(new TextMessage("Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> kembali(String idLine) {
        this.clearResponses();
        this.addResponse(new TextMessage("Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> batalConfirmation(String idLine) {
        return this.help(idLine);
    }

    @Override
    public List<Message> kembaliConfirmation(String idLine) {
        return this.help(idLine);
    }

    @Override
    public List<Message> cekPromo(String idLine) {
        this.clearResponses();
        this.addResponse(new TextMessage("Anda harus memesan terlebih dahulu. Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> gunakanPromo(String idLine, String promo) {
        this.clearResponses();
        this.addResponse(new TextMessage("Anda harus memesan terlebih dahulu. Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> hapusPromo(String idLine) {
        this.clearResponses();
        this.addResponse(new TextMessage("Anda harus memesan terlebih dahulu. Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }

    @Override
    public List<Message> cekKeranjang(String idLine) {
        this.clearResponses();
        this.addResponse(new TextMessage("Anda harus memesan terlebih dahulu. Silakan tekan tombol di bawah untuk mulai memesan"));
        this.addResponse(new InitialMenuMessageSupplier().get());
        return this.getResponses();
    }
    
}
