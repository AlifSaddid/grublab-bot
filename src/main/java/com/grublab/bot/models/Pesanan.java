package com.grublab.bot.models;

import java.util.ArrayList;
import java.util.List;

import com.grublab.bot.promo.NoPromo;
import com.grublab.bot.promo.Promo;

public class Pesanan {
    private ArrayList<Menu> orderlist;
    private Promo promo = new NoPromo();
    private String idLine;

    public Pesanan(String idLine) {
        this.idLine = idLine;
        orderlist = new ArrayList<>();
    }

    public void addMenu(Menu menu) {
        orderlist.add(menu);
    }

    public void removeMenu(String menuName) {
        for (Menu menu : orderlist) {
            if (menu.getName().equalsIgnoreCase(menuName)) {
                orderlist.remove(menu);
                break;
            }
        }
    }

    public void cancelOrder() {
        orderlist.clear();
    }

    public List<Menu> getOrderList() {
        return this.orderlist;
    }

    public String getIdLine() {
        return this.idLine;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public Promo getPromo() {
        return this.promo;
    }

    public int getPrice() {
        int price = 0;
        for (Menu menu : this.orderlist) {
            price += menu.getPrice();
        }
        return price;
    }

    public int getPriceWithPromo() {
        return this.promo.calculate(this.getPrice());
    }
}
