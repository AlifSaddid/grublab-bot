package com.grublab.bot.services;

import com.grublab.bot.repository.StateRepository;
import com.grublab.bot.states.State;
import io.micrometer.core.annotation.Timed;
import org.springframework.stereotype.Service;

@Service
public class StateServiceImpl implements StateService {

    @Timed("state_setuserstate_service")
    public void setUserState(String idLine, State state) {
        StateRepository.setStateByIdLine(idLine, state);
    }

    @Timed("state_getuserstate_service")
    public State getUserStateByIdLine(String idLine) {
        return StateRepository.getUserStateOrCreate(idLine);
    }

}
