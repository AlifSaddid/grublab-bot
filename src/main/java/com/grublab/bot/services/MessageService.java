package com.grublab.bot.services;

import com.linecorp.bot.model.message.Message;

public interface MessageService {

    public void sendMessageToSeller(Message message);

    public void pushMessage(Message message, String idLine);

}
