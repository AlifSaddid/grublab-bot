package com.grublab.bot.services;

import com.grublab.bot.states.State;

public interface StateService {

    public void setUserState(String idLine, State state);

    public State getUserStateByIdLine(String idLine);

}
