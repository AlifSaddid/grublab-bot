package com.grublab.bot.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grublab.bot.models.Menu;
import io.micrometer.core.annotation.Timed;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@Setter
public class MenuServiceImpl implements MenuService {

    private String api;

    private RestTemplate restTemplate;

    private ObjectMapper mapper;

    public MenuServiceImpl() {
        this.setApi(System.getenv("api"));
        this.setRestTemplate(new RestTemplate());
        this.setMapper(new ObjectMapper());
    }

    @Timed("menu_getall_service")
    public List<Menu> getMenus() {
        String url = api + "/v1/menus/";
        return mapper.convertValue(
                restTemplate.getForObject(
                        url,
                        ArrayList.class
                ), new TypeReference<List<Menu>>() {}
        );
    }

    @Timed("Menu_getbyname_service")
    public Menu getMenuByName(String menu) {
        String url = api + "/v1/menus/" + menu;
        return restTemplate.getForObject(
                url,
                Menu.class
        );
    }

}
