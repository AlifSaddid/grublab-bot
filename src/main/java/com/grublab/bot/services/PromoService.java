package com.grublab.bot.services;

import com.grublab.bot.promo.Promo;

import java.util.List;

public interface PromoService {

    public Promo getPromoByName(String name);

    public List<Promo> getPromos();

}
