package com.grublab.bot.services;

import com.grublab.bot.models.Customer;
import com.linecorp.bot.client.LineMessagingClient;
import io.micrometer.core.annotation.Timed;
import lombok.Setter;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ExecutionException;

@Service
@Setter
public class UserServiceImpl implements UserService {

    private String api;

    private LineMessagingClient lineMessagingClient;

    private RestTemplate restTemplate;

    public UserServiceImpl() {
        this.setApi(System.getenv("api"));
        this.setLineMessagingClient(LineMessagingClient
                .builder(System.getenv("GRUBLAB_CHANNEL_TOKEN"))
                .build());

        this.setRestTemplate(new RestTemplate());
    }

    @Timed("customer_getorcreate_service")
    @Override
    public Customer getOrCreateCustomerByIdLine(String idLine) {
        Customer customer;

        String url = api + "/v1/customers/" + idLine;
        try {
            customer = restTemplate.getForObject(
                    url,
                    Customer.class
            );
        } catch (HttpClientErrorException e) {
            customer = createCustomer(idLine);
        }
        customer.setDisplayName(getDisplayName(idLine));
        return customer;
    }

    @Timed("customer_create_service")
    @Override
    public Customer createCustomer(String idLine) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<String> request = new HttpEntity<>(
                    new JSONObject()
                            .put("idLine", idLine)
                            .put("point", 0)
                            .toString(),
                    headers
            );

            String url = api + "/v1/customers/";

            return restTemplate.postForObject(
                    url,
                    request,
                    Customer.class
            );
        } catch (Exception e) {
            return null;
        }
    }

    @Timed("customer_updatepoint_service")
    @Override
    public void updateCustomerPoint(String idLine, int point) {
        try {
            String body = "{\"point\": " + point + "}";
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(api + "/v1/customers/" + idLine))
                    .header("Content-Type", "application/json")
                    .method("PUT", HttpRequest.BodyPublishers.ofString(body))
                    .build();

            client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            return ;
        }
    }

    @Timed("customer_getDisplayName_service")
    public String getDisplayName(String idLine) {
        try {
            return lineMessagingClient
                    .getProfile(idLine)
                    .get()
                    .getDisplayName();
        } catch (InterruptedException | ExecutionException | RuntimeException e) {
            return "";
        }
    }
}
