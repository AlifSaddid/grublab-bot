package com.grublab.bot.services;

import com.grublab.bot.models.Pesanan;
import com.grublab.bot.models.PesananHistory;

import java.util.List;

public interface PesananService {

    public void savePesanan(Pesanan pesanan, String idLine);

    public List<PesananHistory> getCustomerOrderHistory(String idLine);

}
