package com.grublab.bot.services;

import com.grublab.bot.models.Customer;

public interface UserService {

    public Customer getOrCreateCustomerByIdLine(String idLine);

    public void updateCustomerPoint(String idLine, int point);

    public Customer createCustomer(String idLine);

    public String getDisplayName(String idLine);

}
