package com.grublab.bot.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grublab.bot.models.Menu;
import com.grublab.bot.models.Pesanan;
import com.grublab.bot.models.PesananHistory;
import io.micrometer.core.annotation.Timed;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;


@Service
@Setter
public class PesananServiceImpl implements PesananService {

    private String api;

    private RestTemplate restTemplate;

    private ObjectMapper mapper;

    private UserService userService;

    public PesananServiceImpl() {
        this.setApi(System.getenv("api"));
        this.setRestTemplate(new RestTemplate());
        this.setMapper(new ObjectMapper());
        this.setUserService(new UserServiceImpl());
    }

    @Timed("orders_gethistory_service")
    @Override
    public List<PesananHistory> getCustomerOrderHistory(String idLine) {
        String url = api + "/v1/orders/" + idLine;
        return mapper.convertValue(
                restTemplate.getForObject(
                        url,
                        ArrayList.class
                ), new TypeReference<List<PesananHistory>>() {}
        );
    }

    @Timed("orders_savehistory_service")
    @Override
    public void savePesanan(Pesanan pesanan, String idLine) {
        try {
            String body = createPesananBody(pesanan, idLine);
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(api + "/v1/orders/" + idLine))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .build();

            client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Timed("orders_createbody_service")
    public String createPesananBody(Pesanan pesanan, String idLine) {
        StringBuilder json = new StringBuilder("{\"menus\": [");
        for (int i = 0; i < pesanan.getOrderList().size(); i++) {
            Menu menu = pesanan.getOrderList().get(i);
            json.append(String.format("{" +
                    "\"name\": \"%s\"" +
                    "}", menu.getName()));
            if (i != pesanan.getOrderList().size() - 1) {
                json.append(",");
            }
        }
        json.append("],");
        json.append(String.format("\"displayName\": \"%s\",", userService.getDisplayName(idLine)));
        json.append(String.format("\"promo\": \"%s\"", pesanan.getPromo().getName()));
        json.append("}");
        return json.toString();
    }
}

