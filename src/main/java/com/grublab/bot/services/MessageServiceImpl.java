package com.grublab.bot.services;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import io.micrometer.core.annotation.Timed;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

    private LineMessagingClient lineMessagingClient = LineMessagingClient
            .builder(System.getenv("GRUBLAB_CHANNEL_TOKEN"))
            .build();

    @Timed("message_sendToSeller_service")
    @Override
    public void sendMessageToSeller(Message message) {
        try {
            PushMessage pushMessage = new PushMessage(
                    System.getenv("SELLER_ID"),
                    message
            );
            lineMessagingClient
                    .pushMessage(pushMessage)
                    .get();
        } catch (Exception e) {

        }
    }

    @Timed("message_push_service")
    @Override
    public void pushMessage(Message message, String idLine) {
        try {
            PushMessage pushMessage = new PushMessage(
                    idLine,
                    message
            );
            lineMessagingClient
                    .pushMessage(pushMessage)
                    .get();
        } catch (Exception e) {

        }
    }
}
