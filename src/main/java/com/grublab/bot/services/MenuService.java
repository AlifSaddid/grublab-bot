package com.grublab.bot.services;

import com.grublab.bot.models.Menu;

import java.util.List;

public interface MenuService {

    public List<Menu> getMenus();

    public Menu getMenuByName(String menu);

}
