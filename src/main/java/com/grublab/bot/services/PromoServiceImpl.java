package com.grublab.bot.services;

import com.grublab.bot.promo.*;
import io.micrometer.core.annotation.Timed;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PromoServiceImpl implements PromoService {

    @Timed("promo_getbyname_service")
    @Override
    public Promo getPromoByName(String promo) {
        switch (promo) {
            case "Tidak Menggunakan Promo":
                return new NoPromo();
            case "Promo Di Rumah Aja":
                return new PromoDiRumahAja();
            case "Promo Lebaran":
                return new PromoLebaran();
            case "Promo PSBB":
                return new PromoPSBB();
            case "Promo Murah Abis":
                return new PromoMurahAbis();
            case "Promo Ramadan":
                return new PromoRamadan();
            case "Promo Waktu Indonesia Belanja":
                return new PromoWaktuIndonesiaBelanja();
        }
        return null;
    }

    @Timed("promo_getall_service")
    @Override
    public List<Promo> getPromos() {
        List<Promo> promos = new ArrayList<>();
        promos.add(new PromoDiRumahAja());
        promos.add(new PromoLebaran());
        promos.add(new PromoMurahAbis());
        promos.add(new PromoPSBB());
        promos.add(new PromoRamadan());
        promos.add(new PromoWaktuIndonesiaBelanja());

        return promos;
    }
}
