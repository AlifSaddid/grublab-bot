package com.grublab.bot.controllers;

import com.grublab.bot.services.MessageService;
import com.grublab.bot.services.StateService;
import com.grublab.bot.states.State;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@LineMessageHandler
public class LineMessageController {

    LineMessagingClient lineMessagingClient;

    private StateService stateService;

    private MessageService messageService;

    private static final Logger LOGGER = Logger.getLogger(LineMessageController.class.getName());

    @Autowired
    public LineMessageController(final LineMessagingClient lineMessagingClient,
                                 final StateService stateService,
                                 final MessageService messageService) {
        this.lineMessagingClient = lineMessagingClient;
        this.stateService = stateService;
        this.messageService = messageService;
    }

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
        Source source = messageEvent.getSource();
        String idLine = source.getUserId();
        String replyToken = messageEvent.getReplyToken();

        State state = stateService.getUserStateByIdLine(idLine);

        reply(replyToken, state.help(idLine));
    }

    @EventMapping
    public void handlePostbackEvent(PostbackEvent postbackEvent) throws JSONException {
        List<Message> responses = new ArrayList<>();

        String replyToken = postbackEvent.getReplyToken();
        Source source = postbackEvent.getSource();
        String idLine = source.getUserId();
        String data = postbackEvent.getPostbackContent().getData();
        JSONObject jsonData = new JSONObject(data);
        String command = jsonData.getString("command");

        State state = stateService.getUserStateByIdLine(idLine);
        switch (command) {
            case "help":
                responses = state.help(idLine);
                break;
            case "order":
                responses = state.pesan(idLine);
                break;
            case "cekHistory":
                responses = state.cekHistory(idLine);
                break;
            case "cekProfil":
                responses = state.cekProfil(idLine);
                break;
            case "usePromo":
                String namaPromo = jsonData.getString("name");
                responses = state.gunakanPromo(idLine, namaPromo);
                break;
            case "cekKeranjang":
                responses = state.cekKeranjang(idLine);
                break;
            case "tambahPesanan":
                String menu = jsonData.getString("name");
                responses = state.tambahPesanan(idLine, menu);
                break;
            case "hapusPesanan":
                String menuName = jsonData.getString("name");
                responses = state.hapusPesanan(idLine, menuName);
                break;
            case "hapusPromo":
                responses = state.hapusPromo(idLine);
                break;
            case "backConfirmation":
                responses = state.kembaliConfirmation(idLine);
                break;
            case "cancelConfirmation":
                responses = state.batalConfirmation(idLine);
                break;
            case "sendKonfirmasi":
                responses = state.sendKonfirmasi(idLine);
                break;
            case "konfirmasi":
                responses = state.konfirmasi(idLine);
                break;
            case "batal":
                responses = state.batal(idLine);
                break;
            case "kembali":
                responses = state.kembali(idLine);
                break;
            case "orderDone":
                String clientIdLine = jsonData.getString("idLine");
                state = stateService.getUserStateByIdLine(clientIdLine);
                state.konfirmasi(clientIdLine);
                messageService.pushMessage(new TextMessage("Pesanan anda sudah selesai dibuat!"), clientIdLine);
                break;
            default:
                responses = state.help(idLine);
        }

        reply(replyToken, responses);
    }

    private void reply(String replyToken, List<Message> messages) {
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, messages))
                    .get();
        } catch(Exception e) {
            LOGGER.log(Level.SEVERE, "An Error Occured", e);
        }
    }
}
